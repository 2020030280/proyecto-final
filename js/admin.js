// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import {getDatabase, onValue,ref,set,child,get,update,remove} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import{getStorage, ref as refS, uploadBytes, getDownloadURL} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA7XYFm3QywoS7O-a62S3q0t0Mt3vxkSoo",
  authDomain: "proyectofinal-18eaf.firebaseapp.com",
  projectId: "proyectofinal-18eaf",
  storageBucket: "proyectofinal-18eaf.appspot.com",
  messagingSenderId: "599064225325",
  appId: "1:599064225325:web:97265210dacf65b97e3aa4",
  databaseURL: "https://proyectofinal-18eaf-default-rtdb.firebaseio.com/"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

var btnAgregar=document.getElementById('btnAgregar');
var btnBorrar=document.getElementById('btnBorrar');
var btnActualizar= document.getElementById('btnActualizar');
var btnBuscar= document.getElementById('btnBuscar');
var archivo= document.getElementById('archivo');
var verImagen= document.getElementById('verImagen');

var produc=document.getElementById('ppp');
var id="";
var Nombre= "";
var des="";
var precio="";
var nombreimg="";
var url="";

function leer(){
  id=document.getElementById('ID').value;
  Nombre=document.getElementById('nombre').value;
  des=document.getElementById('des').value;
  precio=document.getElementById('precio').value;
  nombreimg= document.getElementById('imgNombre').value;
  url= document.getElementById('URL').value;
}

function escribirinputs(){
  document.getElementById('ID').value=id;
  document.getElementById('nombre').value=Nombre;
  document.getElementById('des').value= des;
  document.getElementById('precio').value=precio;
  document.getElementById('imgNombre').value=nombreimg;
  document.getElementById('URL').value=url;
}

function insertar(){
  leer();
  set(ref(db,"Productos/" + id),{
 nombre:Nombre,
  descripcion:des,
  precio:precio,
  Status:'0',
  nombreimg: nombreimg,
  url: url

  }).then((docRef)=>{
      alert("Se agrego el registro con exito");
      mostrarDatos();
      limpiar();
  
  }).catch((error)=>{
      alert("Surgio un error", error);
  })

};
function mostrarDatos(){
  leer();
  const dbref= ref(db);
  produc.innerHTML="";
  get(child(dbref,'Productos/' + id)).then((snapshot)=>{
      if(snapshot.exists()){
          Nombre= snapshot.val().nombre;
          des = snapshot.val().descripcion;
          
          precio=snapshot.val().precio;
          nombreimg= snapshot.val().nombreimg;
          url= snapshot.val().url;
          if(snapshot.val().Status=="0"){
            escribirinputs();
            
            produc.innerHTML= produc.innerHTML +"<div id='productos'>" + id +     
            "<img src='"+url+"' alt=''>" +
            "<div id='pro'> <h3>" + Nombre + "</h3> <p>Descripcion: <br>"+des +
            "</p><br><a href=''>$"+precio+"MXN</a></div><br>"+
            "<button>Comprar</button></div>";
            }else{
              limpiar();
            }
      }else{
          alert("No existe la id");
      }
  }).catch((error)=>{
      alert("Surgio un error" + error);
  })
}
function actualizar(){
  leer();
  update(ref(db, 'Productos/' + id),{
    nombre:Nombre,
    descripcion:des,
    precio:precio,
    nombreimg: nombreimg,
    url: url
  }).then(()=>{
      alert("Se realizó actualizacion");
      mostrarDatos();
      limpiar();
  }).catch(() => {
      alert("Surgio un error" + error);
      limpiar();
  });
}

function deshabilitar(){
  leer();
  update(ref(db, 'Productos/' + id),{
    Status:"1"
  }).then(()=>{
      alert("Se deshabilitó");
      mostrarDatos();
      limpiar();
  }).catch(() => {
      alert("Surgio un error" + error);
  });
}

function limpiar(){
  id="";
  Nombre="";
  precio="";
  des="";
  nombreimg="";
  url="";
  escribirinputs();
}
function cargarImagen(){
  const file= event.target.files[0];
  const name= event.target.files[0].name;

  const storage= getStorage();
  const storageRef= refS(storage, 'imagenes/' + name);

  uploadBytes(storageRef, file).then((snapshot) => {
    document.getElementById('imgNombre').value=name;

    alert('se cargo la imagen');
  });
}

function descargarImagen(){
  archivo= document.getElementById('imgNombre').value;
  // Create a reference to the file we want to download
const storage = getStorage();
const starsRef = refS(storage, 'imagenes/' + archivo);

// Get the download URL
getDownloadURL(starsRef)
  .then((url) => {
   document.getElementById('URL').value=url;
   document.getElementById('imagen').src=url;
  })
  .catch((error) => {
    // A full list of error codes is available at
    // https://firebase.google.com/docs/storage/web/handle-errors
    switch (error.code) {
      case 'storage/object-not-found':
        console.log("No existe el archivo");
        break;
      case 'storage/unauthorized':
        console.log("No tiene permisos");
        break;
      case 'storage/canceled':
        console.log("No existe conexion con la base de datos")
        break;

      // ...

      case 'storage/unknown':
        console.log("Ocurrio algo inesperado")
        break;
    }
  });
}
  btnAgregar.addEventListener('click', insertar);
  btnBuscar.addEventListener('click', mostrarDatos);
  btnActualizar.addEventListener('click', actualizar);
  btnBorrar.addEventListener('click', deshabilitar);
  archivo.addEventListener('change', cargarImagen);
  verImagen.addEventListener('click', descargarImagen);