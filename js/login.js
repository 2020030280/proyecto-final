import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
    import{getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

    const firebaseConfig = {
        apiKey: "AIzaSyA7XYFm3QywoS7O-a62S3q0t0Mt3vxkSoo",
        authDomain: "proyectofinal-18eaf.firebaseapp.com",
        projectId: "proyectofinal-18eaf",
        storageBucket: "proyectofinal-18eaf.appspot.com",
        messagingSenderId: "599064225325",
        appId: "1:599064225325:web:97265210dacf65b97e3aa4",
        databaseURL: "https://proyectofinal-18eaf-default-rtdb.firebaseio.com/"
      };
      const app = initializeApp(firebaseConfig);
      var btnLogin= document.getElementById('btnLogin');
  var btnLimpiar= document.getElementById('btnLimpiar');
  
  function acceso(){
    let email = document.getElementById('Usuario').value;
    let contra= document.getElementById('contra').value;

    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, contra)
    .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;
    location.href="/html/administrador.html";
     })
    .catch((error) => {
    location.href="/html/error.html";
    });
  }

  function limpiar(){
    document.getElementById('Usuario').value="";
    document.getElementById('contra').value="";
  }

  btnLogin.addEventListener('click', acceso);
  btnLimpiar.addEventListener('click', limpiar);